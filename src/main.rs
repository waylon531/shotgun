use std::io::{self,Read,BufReader, BufWriter};
use serde::de::Deserializer;
use erased_serde;

use structopt::StructOpt;

enum Format {
    Cbor,
    Json,
}

struct ParseFormatError;

impl std::fmt::Debug for ParseFormatError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "unknown format")
    }
}

impl std::string::ToString for ParseFormatError {
    fn to_string(&self) -> String {
        String::from("unknown format")
    }
}

impl std::str::FromStr for Format {
    type Err = ParseFormatError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "cbor" => Ok(Self::Cbor),
            "json" => Ok(Self::Json),
            _ => Err(ParseFormatError)
        }
    }
}

#[derive(StructOpt)]
struct Args {
    #[structopt(short = "i", long = "input")]
    input: Format,

    #[structopt(short = "o", long = "output")]
    output: Format,
}

fn main() {
    let args = Args::from_args();

    // Why
    let si = std::io::stdin();
    let so = std::io::stdout();
    //let sil = si.lock();
    let sol = so.lock();
    //let i = BufReader::new(sil);
    let o = BufWriter::new(sol);

    // These need to live for the entire scope of the program because pain
    // Let us recieve many locks on stdin
    let si_clone = io::stdin();
    // This is a static string because it's 1 less lifetime to worry about
    let strstr: &'static str = "this is a string";
    let mut cbor_lmao = serde_cbor::Deserializer::from_slice(strstr.as_bytes());
    let cbor = &mut cbor_lmao;
    let mut json_lmao = serde_json::Deserializer::from_str(strstr);
    let json = &mut json_lmao;

    //let mut de = get_de(args.input, &i);
    let mut de: Box<dyn erased_serde::Deserializer> = match args.input {
        Format::Cbor => {
            //panic!("not implemented because types are hard")
            Box::new(erased_serde::Deserializer::erase(cbor))
        },
        Format::Json => {
            //panic!("not implemented because types are hard")
            Box::new(erased_serde::Deserializer::erase(json))
        }
    };
    let mut ser = match args.output {
        Format::Cbor => {
            panic!("not implemented because types are hard")
            //serde_cbor::Serializer::new(serde_cbor::ser::IoWrite::new(o))
        },
        Format::Json => {
            serde_json::Serializer::pretty(o)
            //panic!("not implemented because types are hard")
        }
    };

    serde_transcode::transcode(de, &mut ser).unwrap();
}
